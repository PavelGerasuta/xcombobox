﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppXComboBox
{
    public class XComboBox : Control
    {
            private int Ypoint;

            private bool Flag = false;

            private bool FlagCheck = false;

            private double d_y;

            private int Triangle;

            private int newHeight;


            public int Count { get; set; }

            public int Min { get; set; }

            public int Max { get; set; }

            public int Point { get; set; }

            public Point[] points = new Point[3];

            public int now_Point { get; set; }

            public List<string> _Params = new List<string>();

            private EventHandler EventItemSelect;


            public void AddParam(string param)
            {
                
                _Params.Add(param);
                newHeight += 12;
                Refresh();
             

            }


            public XComboBox()
            {

                newHeight = Height + 32;

                AddParam("C#");
                AddParam("Java");
                AddParam("C++");

            }
          


            public void DeleteParamAtIndex(int index)
            {
                      _Params.RemoveAt(index);
                      Refresh(); 
            }

            public void ClearParam()
            {
                _Params.Clear();
                Refresh();
            }

            protected void Do()
            {
                EventItemSelect?.Invoke(this, EventArgs.Empty);
            }

        

            public event EventHandler OnItemSelect
            {
                add { EventItemSelect += value; }
                remove { EventItemSelect -= value; }
            }

            protected override void OnMouseClick(MouseEventArgs e)
            {
                base.OnMouseClick(e);


                if ((e.Y > 0) & (e.Y < 20) & (e.X < points[2].X) & (e.X > points[0].X) & Flag == false)
                {
                    Flag = true;
                    Height = newHeight;
                    Refresh();

                }else if ((e.Y > 22) & (e.Y < (_Params.Count) * 12 + newHeight) & (e.X < Width) & (e.X > 0))
                {
                    FlagCheck = true;
                    Ypoint = e.Y;
                    Refresh();
                    Do();
                    
                }
                else
                {
                    Flag = false;
                    Height = 20;
                    Refresh();
                }

               





            }



            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                Graphics g = e.Graphics;

                Triangle = Width - 15;

                points[0].X = Triangle; points[0].Y = 8;
                points[1].X = Triangle + 5; points[1].Y = 15;
                points[2].X = Triangle + 10; points[2].Y = 8;

                SolidBrush blackBrush = new SolidBrush(Color.Black);


                g.DrawRectangle(new Pen(Brushes.Black, 2), 0, 0, Width, 22);

                g.FillPolygon(blackBrush, points);


                if (Flag)
                {

                    g.DrawRectangle(new Pen(Brushes.Black, 1), 0, 20, Width, Height);

                    int k = 20;

                    for (int i = 0; i < _Params.Count; i++)
                    {
                        g.DrawString(_Params[i], new Font("Times New Roman", 10),
                           Brushes.Black, 0, k);
                        k += 12;
                    }


                if (FlagCheck == true)
                    {
                        int i_count_now = _Params.Count * (k);
                        d_y = Ypoint / 12-2;
                        double d_y_rec = Ypoint / 12;

                        if ((Ypoint < i_count_now) & (Ypoint > 0) & (d_y < _Params.Count))
                        {

                            g.FillRectangle(new SolidBrush(Color.FromArgb(130, 0, 0, 255)), 0, (int)d_y_rec * 12-3, Width, 13);

                            now_Point = (int)d_y;

                            g.DrawString(_Params[now_Point], new Font("Times New Roman", 10), Brushes.Black, 0, 0);
                        
                        }

                    }
                

                }
            }

        }
    }
