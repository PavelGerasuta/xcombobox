﻿
namespace WindowsFormsAppXComboBox
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxEventChek = new System.Windows.Forms.TextBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.xComboBox1 = new WindowsFormsAppXComboBox.XComboBox();
            this.textBoxNameParam = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxEventChek
            // 
            this.textBoxEventChek.Location = new System.Drawing.Point(61, 12);
            this.textBoxEventChek.Name = "textBoxEventChek";
            this.textBoxEventChek.ReadOnly = true;
            this.textBoxEventChek.Size = new System.Drawing.Size(121, 20);
            this.textBoxEventChek.TabIndex = 2;
            this.textBoxEventChek.TextChanged += new System.EventHandler(this.textBoxEventChek_TextChanged);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(277, 13);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(106, 23);
            this.buttonAdd.TabIndex = 3;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(277, 42);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(106, 23);
            this.buttonDelete.TabIndex = 4;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(277, 71);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(106, 23);
            this.buttonClear.TabIndex = 5;
            this.buttonClear.Text = "Очистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // xComboBox1
            // 
            this.xComboBox1.Count = 0;
            this.xComboBox1.Location = new System.Drawing.Point(61, 71);
            this.xComboBox1.Max = 0;
            this.xComboBox1.Min = 0;
            this.xComboBox1.Name = "xComboBox1";
            this.xComboBox1.now_Point = 0;
            this.xComboBox1.Point = 0;
            this.xComboBox1.Size = new System.Drawing.Size(121, 21);
            this.xComboBox1.TabIndex = 1;
            this.xComboBox1.Text = "xComboBox1";
            // 
            // textBoxNameParam
            // 
            this.textBoxNameParam.Location = new System.Drawing.Point(428, 11);
            this.textBoxNameParam.Name = "textBoxNameParam";
            this.textBoxNameParam.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameParam.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxNameParam);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.textBoxEventChek);
            this.Controls.Add(this.xComboBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private XComboBox xComboBox1;
        private System.Windows.Forms.TextBox textBoxEventChek;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.TextBox textBoxNameParam;
    }
}

