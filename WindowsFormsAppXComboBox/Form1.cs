﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppXComboBox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            xComboBox1.OnItemSelect += new EventHandler(textBoxEventChek_TextChanged);
        }

        private void textBoxEventChek_TextChanged(object sender, EventArgs e)
        {
           
                textBoxEventChek.Text = (xComboBox1._Params[xComboBox1.now_Point]);
           
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {

            if (textBoxNameParam.Text != "")
            {
                xComboBox1.AddParam(textBoxNameParam.Text);
            }
            else
            {
                string message = "Введите сообщение";
                string caption = "Ошибка";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                result = MessageBox.Show(message, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    this.Close();
                }
            }

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            xComboBox1.DeleteParamAtIndex(xComboBox1.now_Point);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            xComboBox1.ClearParam();
        }
    }
}
